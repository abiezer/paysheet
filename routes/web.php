<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('adminlte::auth.login');
})->middleware('guest');

Route::group(['middleware' => 'auth'], function () {
//    Route::get('/link1', function ()    {
//
// Uses Auth Middleware
//    });
//Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
#adminlte_routes
});

Route::group(['middleware' => 'web'], function () {
    Route::resource('employee','EmployeeController');
    Route::get('import_payments/{year}/{period}','EmployeeController@import_payments');
    Route::get('receipt/{period}/{year}','EmployeeController@receipt')->name('receipt.receipt');
    Route::get('import_employees','EmployeeController@import_employees');
    Route::get('period','EmployeeController@period');
    Route::get('import_periods','EmployeeController@import_periods');
    Route::get('first_link','EmployeeController@first_link');
    Route::get('import_data','EmployeeController@import_data');
    Route::get('get_periods_import/{type_paysheet}','EmployeeController@get_periods_import');
});