<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Payment extends Model
{
    use SoftDeletes;
    
    protected $table = 'payments';
    protected $dates = ['deleted_at'];
}
