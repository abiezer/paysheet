<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Payment;
use App\Period;
use \PDO as PDO;
use \DateTime as DateTime;
use Barryvdh\DomPDF\Facade as PDF;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }

    public function import_employees(){

        $result1 = Personal("SELECT * FROM Datos_Trabajador");
        while($trabajadores = $result1->fetch()){
            $banco = get_dates($trabajadores['Codigo_Banco'],'bank');
            $appoinment = get_cargo(intval($trabajadores['Codigo_Cargo']));
            $unity = get_unity($trabajadores['Centro_Costo']);
            $name = utf8_encode(strval($trabajadores["Nombre"] . " " . $trabajadores["Apellido"]));
            $date = new DateTime($trabajadores['Fecha_Ingreso']);

            $trabajador = [
                "identification"    => intval($trabajadores["Cedula"]),
                "name"              => $name,
                "index_card"        => intval($trabajadores['Ficha']),
                "unity"             => $unity,
                "appoinment"        => $appoinment,
                "salary"            => floatval($trabajadores['Sueldo']),
                "bank"              => $banco,
                "bank_account"      => utf8_encode(strval($trabajadores['Cuenta_Banco'])),
                "type_paysheet"     => intval($trabajadores['Tipo_Nomina']),
                "join_date"         => $date->format('Y-m-d')
            ];

            $validator = Validator::make($trabajador, [
                'identification' => 'unique:employees',
            ]);
            
            if ($validator->fails()) {
                continue;
            }else{
                $employee = new Employee;
                $employee->identification = $trabajador['identification'];
                $employee->name = $trabajador['name'];
                $employee->index_card = $trabajador['index_card'];
                $employee->unity = $trabajador['unity'];
                $employee->appoinment = $trabajador['appoinment'];
                $employee->salary = $trabajador['salary'];
                $employee->bank = $trabajador['bank'];
                $employee->bank_account = $trabajador['bank_account'];
                $employee->type_paysheet = $trabajador['type_paysheet'];
                $employee->join_date = $trabajador['join_date'];
                $employee->save();
            }
        }
    }

    public function import_periods(){
        $fecha_pre = NominaM("SELECT PERIODO,FDESDE,FHASTA,OBSERV,ANO FROM NO012D");        
        while($fecha = $fecha_pre->fetch()){
            $since_pre = new DateTime($fecha['FDESDE']);
            $since = $since_pre->format('Y-m-d');

            $until_pre = new DateTime($fecha['FHASTA']);
            $until = $until_pre->format('Y-m-d');

            $observation = utf8_encode($fecha['OBSERV']);

                $phase_query = Period::where('until',$until)
                ->where('since',$since)
                ->where('year',$fecha['ANO'])
                ->first();
                
                if($phase_query){
                    continue;
                }else{
                    $phase = new Period;
                    $phase->period = intval($fecha['PERIODO']);
                    $phase->since = $since;
                    $phase->until = $until;
                    $phase->type_paysheet = "m";
                    $phase->description = $observation;
                    $phase->year = $fecha['ANO'];
                    $phase->save();
                }
        }

            $fecha_pre = NominaD("SELECT PERIODO,FDESDE,FHASTA,OBSERV,ANO FROM NO012D");        
            while($fecha = $fecha_pre->fetch()){
            $since_pre = new DateTime($fecha['FDESDE']);
            $since = $since_pre->format('Y-m-d');

            $until_pre = new DateTime($fecha['FHASTA']);
            $until = $until_pre->format('Y-m-d');

            $observation = utf8_encode($fecha['OBSERV']);

                $phase_query = Period::where('until',$until)
                ->where('since',$since)
                ->first();
                if($phase_query){
                    continue;
                }else{
                    $phase = new Period;
                    $phase->period = intval($fecha['PERIODO']);
                    $phase->since = $since;
                    $phase->until = $until;
                    $phase->type_paysheet = "d";
                    $phase->description = $observation;
                    $phase->year = $fecha['ANO'];
                    $phase->save();
                }
            }
    }
    public function import_payments($year_ext,$period_ext){
       
        $result2 = NominaM("SELECT * FROM PagosNomina WHERE (ANO='$year_ext') and (PERIODO='$period_ext');");

        $count_m_no = 0;
        $count_m_si = 0;

        while($pagos = $result2->fetch()){
            $trabajador = [
                "identification"    =>  intval($pagos['CEDULA'])
            ];
            
            $validator = Validator::make($trabajador, [
                'identification' => 'unique:employees',
            ]);

            if($validator->fails()){
                // $phase = get_period($pagos['ANO'],$pagos['PERIODO']);
                $quantity = get_quantity($pagos['REFER']);
                $surcharge = get_ref($pagos['PERIODO']);
                $assignment = round(floatval($pagos['ASIGNA']),2);
                $deduction = round(floatval($pagos['DEDUCC']),2);
                if($quantity < 1){
                    $value_ref=0;
                }else{
                    if($assignment>0){
                        $value_ref = round(floatval($assignment / $quantity),2);
                    }else{
                        $value_ref = round(floatval($deduction / $quantity),2);
                    }
                }
                
                $pago = [
                    "period"                    => intval($pagos['PERIODO']),
                    "code"                      => utf8_encode(strval($pagos['CONCEPTO'])),
                    "concept"                   => get_dates($pagos['CONCEPTO'],"concept"),
                    "surcharge"                 => $surcharge,
                    "quantity"                  => $quantity,
                    "value_ref"                 => $value_ref,
                    "assignment"                => $assignment,
                    "deduction"                 => $deduction,
                    "balance"                   => utf8_encode(strval(0)),
                    "employee_identification"   => intval($pagos['CEDULA'])
                ];

                $period = Period::select('id')
                ->where('year',$pagos['ANO'])
                ->where('period',$pago['period'])
                ->where('type_paysheet','m')
                ->first();
                                
                $payment = Payment::where('employee_identification',$pago['employee_identification'])
                ->where('code',$pago['code'])
                ->where('period_id',$period->id)
                ->first();

                if($payment){
                    echo "(Mensual) no se guardo" . $count_m_no."<br><br>";
                    $count_m_no++;
                    continue;
                }else{
                    echo "(Mensual) si se guardo" . $count_m_si."<br><br>";
                    $count_m_si++;
                    $payment = new Payment;
                    $payment->code        = $pago['code'];
                    $payment->concept     = $pago['concept'];
                    $payment->surcharge   = $pago['surcharge'];
                    $payment->quantity    = $pago['quantity'];
                    $payment->value_ref   = $pago['value_ref'];
                    $payment->assignment  = $pago['assignment'];
                    $payment->deduction   = $pago['deduction'];
                    $payment->balance     = $pago['balance'];
                    $payment->period_id   = intval($period->id);
                    $payment->employee_identification = $pago['employee_identification'];
                    $payment->save();
                }
            }else{
                continue;
            }//if
        }//while

        $count_d_si = 0;
        $count_d_no = 0;
        
        $result2 = NominaD("SELECT * FROM PagosNomina WHERE (ANO='$year_ext') and (PERIODO='$period_ext');");
        
        while($pagos = $result2->fetch()){
            $trabajador = [
                "identification"    => $pagos['CEDULA']
            ];
            
            $validator = Validator::make($trabajador, [
                'identification' => 'unique:employees',
            ]);

            if($validator->fails()){
                // $period = get_period($pagos['ANO'],$pagos['PERIODO']);
                $quantity = get_quantity($pagos['REFER']);
                $surcharge = get_ref($pagos['PERIODO']);
                $assignment = round(floatval($pagos['ASIGNA']),2);
                $deduction = round(floatval($pagos['DEDUCC']),2);
                if($quantity < 1){
                    $value_ref=0;
                }else{
                    if($assignment>0){
                        $value_ref = round(floatval($assignment / $quantity),2);
                    }else{
                        $value_ref = round(floatval($deduction / $quantity),2);
                    }
                }

                $pago = [
                    "period"                    => $pagos['PERIODO'],
                    "code"                      => utf8_encode(strval($pagos['CONCEPTO'])),
                    "concept"                   => get_dates($pagos['CONCEPTO'],"concept"),
                    "surcharge"                 => $surcharge,
                    "quantity"                  => $quantity,
                    "value_ref"                 => $value_ref,
                    "assignment"                => $assignment,
                    "deduction"                 => $deduction,
                    "balance"                   => utf8_encode(strval(0)),
                    "employee_identification"   => intval($pagos['CEDULA'])
                ];
                
                $period = Period::select('id')
                ->where('year',$pagos['ANO'])
                ->where('period',$pago['period'])
                ->where('type_paysheet','d')
                ->first();
                
                $payment = Payment::where('employee_identification',$pago['employee_identification'])
                ->where('period_id',$period->id)
                ->where('code',$pago['code'])
                ->first();

                if($payment){
                    echo "(Diaria) no se guardo " . $count_d_no . "<br><br>";
                    $count_d_no++;
                    continue;
                }else{
                    echo "(Diaria) se guardo " . $count_d_si . "<br><br>";
                    $count_d_si++;
                    $payment = new Payment;
                    $payment->code        = $pago['code'];
                    $payment->concept     = $pago['concept'];
                    $payment->surcharge   = $pago['surcharge'];
                    $payment->quantity    = $pago['quantity'];
                    $payment->value_ref   = $pago['value_ref'];
                    $payment->assignment  = $pago['assignment'];
                    $payment->deduction   = $pago['deduction'];
                    $payment->balance     = $pago['balance'];
                    $payment->period_id   = intval($period->id);
                    $payment->employee_identification = $pago['employee_identification'];
                    $payment->save();
                }
            }else{
                continue;
            }//if
        }//while
        return "Se guardaron de la nomina Mensual: " . $count_m_si . "<br>" . "No guardaron de la nomina Mensual: " . $count_m_no . "<br>" . "Se guardaron de la nomina Diaria: " . $count_d_si . "<br>" . "No guardaron de la nomina Diaria: " . $count_d_no . "<br>";
    }//function
/*
identification  -Cedula             = Cedula ->  Datos_Trabajador
name            -Nombre             = Nombre ->  Datos_Trabajador
index_card      -Ficha              = Ficha ->  Datos_Trabajador
unity           -Unidad             = 
appoinment      -Cargo              = Codigo_Cargo -> Datos_Trabajador JOIN
salary          -Salario            = Sueldo -> Datos_Trabajador
bank            -Banco              = Codigo_Banco -> Datos_Trabajador
banck_account   -Cuenta bancaria    = Cuenta_Banco -> Datos_Trabajador
type_paysheet
    
nro_receipt     -Nro de recibo      = 
period          -Periodo
since           -Desde              = ANO -> PagosNomina . PERIODO -> PagosNomina
until           -Hasta              = ANO -> PagosNomina . PERIODO -> PagosNomina
code            -Codigo             = PERIODO -> PagosNomina
concept         -Concepto           = Descripcion_Concepto -> PagosNomina JOIN Conceptos_Generales
surcharge       -Recargo            = 
quantity        -Cantidad           = 
value_ref       -Valor ref          = Refer  -> PagosNomina . PERIODO -> PagosNomina
assignment      -Asignacion         = ASIGNA -> PagosNomina . PERIODO -> PagosNomina
deduction       -Deduccion          = DEDUCC -> PagosNomina . PERIODO -> PagosNomina
balance         -Saldo              = 
*/
    public function get_periods_import($type_paysheet){
            $periods = Period::where('type_paysheet',$type_paysheet)->get();
            return $periods;
    }

    // public function import_for_period($since, $until){
    //     $this->
    // }

    public function import_data(){
        return view('adminlte::import_data');
    }

    public function first_link(){
        $period = Period::where('type_paysheet','m')->orderBy('until','desc')->first();
        return $period;
    }

    public function period(){
        $employee = Employee::select('type_paysheet')->where('identification',Auth::user()->employee_identification)->first();
                
        if($employee->type_paysheet==2)
            // $periods = Period::where('year',date("Y"))->where('type_paysheet','d')->get();
            $periods = Period::where('type_paysheet','m')->orderBy('until','desc')->get();
        else{
            // $periods = Period::where('year',date("Y"))->where('type_paysheet','m')->get();
            $periods = Period::where('type_paysheet','m')->orderBy('until','desc')->get();
        }
        return view('adminlte::period',['periods'=>$periods]);
    }

    public function receipt($period_pre,$year){
        
        $employee = Employee::where('identification',Auth::user()->employee_identification)->first();

        if($employee->type_paysheet==2){
            $period = Period::where('year',$year)->where('period',$period_pre)->where('type_paysheet','d')->first();
        }else{
            $period = Period::where('year',$year)->where('period',$period_pre)->where('type_paysheet','m')->first();
        }
 
        $payments = Payment::where('employee_identification',Auth::user()->employee_identification)
                    ->where('code', '<>', '52')
                    ->where('code', '<>', '55')
                    ->where('code', '<>', '9')
                    ->where('code', '<>', '7')
                    ->where('code', '<>', '159')
                    ->where('code', '<>', '158')
                    ->where('period_id',$period->id)
                    ->get();

        $date_s = new DateTime($period->since);
        $date_u = new DateTime($period->until);
        $date = [
            'since'         =>  $date_s->format('d-m-Y'),
            'until'         =>  $date_u->format('d-m-Y'),
            'observation'   =>  $period->description
        ];

        if($employee){
            $date_i = new DateTime($employee->join_date);
            $fecha_i = $date_i->format('d-m-Y'); 
        }
        if($payments){
            $total_assignment = 0;
            $total_deduction = 0;
            foreach($payments as $payment){
                $total_assignment = $total_assignment + $payment->assignment;
                $total_deduction = $total_deduction + $payment->deduction;
            }
        }
            $overall = [
                'assignment'    =>  $total_assignment,
                'deduction'     =>  $total_deduction,
                'grandson'      =>  $total_assignment - $total_deduction
            ];
            
        $pdf = PDF::loadView('receipt',['employee'=>$employee, 'payments'=>$payments, 'date'=>$date, 'fecha_i' => $fecha_i,'overall'=>$overall]);
        return $pdf->stream();
    }
}