<?php

function Personal($sql){
    $db_username = ''; //username
    $db_password = ''; //password

    //path to database file
    $database_path = "C:\cabelum\Bcodatosnomina\Personal.mdb";
    //check file exist before we proceed
    if (!file_exists($database_path)) {
        die("Access database file not found !");
    }

    //create a new PDO object
    $database = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
    $result = $database->query($sql);
    return $result;
}

function NominaM($sql){
    $db_username = ''; //username
    $db_password = ''; //password

    //path to database file
    $database_path = "C:\cabelum\Bcodatosnomina\NominaM.mdb";
    //check file exist before we proceed
    if (!file_exists($database_path)) {
        die("Access database file not found !");
    }

    //create a new PDO object
    $database = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
    $result1 = $database->query($sql);
    return $result1;
}

function NominaD($sql){
    $db_username = ''; //username
    $db_password = ''; //password

    //path to database file
    $database_path = "C:\cabelum\Bcodatosnomina\NominaD.mdb";
    //check file exist before we proceed
    if (!file_exists($database_path)) {
        die("Access database file not found !");
    }

    //create a new PDO object
    $database = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
    $result1 = $database->query($sql);
    return $result1;
}

function isLeapYear($year){
    return ((($year%4 == 0) && ($year%100)) || $year%400 == 0)? true: false;
}

function get_dates($code,$value){
        if($value =="bank"){
            if($code==1){
                return "Banco Mercantil";
            }elseif($code==2){
                return "Banco Banfoandes";
            }elseif($code==3){
                return "Banco Venezuela";
            }else{
                return "Sin banco";
            }
        }elseif($value=="concept"){
            $sql  = "SELECT * FROM Conceptos_Generales WHERE Codigo_Concepto='$code';";
            $result = NominaM($sql);
            $concept = $result->fetch();
            return  utf8_encode($concept['Descripcion_Concepto']);
        }else{
            return "Value is poorly written";
        }
    }

    function get_period($year,$period){
         $fecha_pre = NominaM("SELECT FDESDE,FHASTA,OBSERV FROM NO012D WHERE (ANO='$year') AND (PERIODO='$period')");
                
        while($fecha = $fecha_pre->fetch()){
            $since_pre = new DateTime($fecha['FDESDE']);
            $since = $since_pre->format('Y-m-d');

            $until_pre = new DateTime($fecha['FHASTA']);
            $until = $until_pre->format('Y-m-d');

            $observation = utf8_encode($fecha['OBSERV']);

            $period = [
                'since'         =>  $since,
                'until'         =>  $until,
                'observation'   =>  $observation
            ];
        }
        return $period;
    }

    function get_cargo($cod_cargo){
        $result = Personal("SELECT Descripcion_cargo FROM Tipo_Cargos WHERE Codigo_Cargo=$cod_cargo");
        $cargo = $result->fetch();
        if($cargo){
            return utf8_encode($cargo['Descripcion_cargo']);
        }else{
            return "sin cargo";
        }
    }

    function get_quantity($quantity_pre){
        $quantity = intval($quantity_pre);
        if($quantity > 1){
            return $quantity;
        }else{
            return 0;
        }
    }

    function get_ref($concepto){
        $result = NominaM("SELECT DESCRIPCION FROM NO002D WHERE CONCEPTO='$concepto';");
        if($result){
            $surcharge = $result->fetch();
            if($surcharge['DESCRIPCION']==""){
                return " ";
            }else{
                return $surcharge['DESCRIPCION'];
            }
        }else{
            return "don't surcharge";
        }
    }

    function get_unity($CentroCosto){
        $result = Personal("SELECT DESCRIPCION_CC FROM Centro_Costo WHERE Codigo_CC='$CentroCosto'");
        $unity = $result->fetch();
        return utf8_encode($unity['DESCRIPCION_CC']);
    }
?>