<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        th{
           background:#9b9b9b;
        }
        .text-center{
            text-align:center;
        }

        .text-left{
            text-align:left;
        }

        .table_space{
            height:2%;
        }

        .table_3{
            border:solid 0.2px;
            border-color:black;
            /*text-align:center;*/
            width:100%;
            border-spacing: 0.1px;
            padding: 0.1px;
            font-size:x-small;
        }
        
        .table_2{
            border:solid 0.2px;
            border-color:black;
            text-align:center;
            width:100%;
            border-spacing: 0.1px;
            padding: 0.1px;
            font-size:x-small;
        }

        .table_1{
            /*border:solid 0.2px;
            border-color:black;*/
            text-align:center;
            width:100%;
            border-spacing: 0.1px;
            padding: 0.1px;
            font-size:x-small;
        }
            .logo{
                padding-top: 10px;
                float:left;
                width:140px;
                height:70;
            }
            .heading{
                font-size:small;
                text-align:center;
            }
    </style>
</head>
<body>
<table>
    <tr>
        <td>
            <img class="logo" src="img/cabelum.jpg" alt="logo_cabelum">
        </td>
        <td>
            <p class="heading"><strong>CONDUCTORES DE ALUMINIO DEL CARONI, C.A</strong></p>
            <p class="heading">Av. Perimetral, Nueva Zona Industrial, Edificio Cabelum. Ciudad Bolívar Edo. Bolívar</p>
            <p class="heading">www.cabelum.gob.ve</p>
        </td>
    </tr>
</table>


    <table class="table_2">
        <tr>
            <th class="table_2">Cedula de Identidad</th>
            <th class="table_2">Nombre</th>
            <th class="table_2">Unidad</th>
            <th class="table_2">Cargo</th>
            <th class="table_2">Recibo</th>
        </tr>
        <tr>
            <td class="table_2">{{$employee->identification}}</td>
            <td class="table_2">{{$employee->name}}</td>
            <td class="table_2">{{$employee->unity}}</td>
            <td class="table_2">{{$employee->appoinment}}</td>
            <td class="table_2">Sin Nro</td>
        </tr>
    </table>

    <br>

    <table class="table_2">
        <tr>
            <th class="table_2" colspan="2">Periodo</th>
            <th rowspan="2" class="table_2">F. Ingreso</th>
            <th rowspan="2" class="table_2">Salario</th>
            <th rowspan="2" class="table_2">Banco</th>
            <th rowspan="2" class="table_2">Nro Cuenta</th>
            <th rowspan="2" class="table_2">Ficha</th>
        </tr>
        <tr>
            <th class="table_2">Desde</th>
            <th class="table_2">Hasta</th>
        </tr>
        <tr>
            <td class="table_2">{{$date['since']}}</td>
            <td class="table_2">{{$date['until']}}</td>
            <td class="table_2">{{$fecha_i}}</td>
            <td class="table_2">{{$employee->salary}}</td>
            <td class="table_2">{{$employee->bank}}</td>
            <td class="table_2">{{$employee->bank_account}}</td>
            <td class="table_2">{{$employee->index_card}}</td>
        </tr>
    </table>

    <br>

    <table class="table_2">
    <tr>
        <th class="table_2">Codigo</th>
        <th class="table_2">Concepto</th>
        <th class="table_2">Recargo</th>
        <th class="table_2">Cantidad</th>
        <th class="table_2">Valor Ref</th>
        <th class="table_2">Asignacion</th>
        <th class="table_2">Deducción</th>
        <th class="table_2">Saldo</th>
    </tr>
    @if($payments)
        @foreach($payments as $payment)
            <tr class="payments">
                <td class="table_1">{{$payment->code}}</td>
                <td class="table_1">{{$payment->concept}}</td>
                <td class="table_1">{{$payment->surcharge}}</td>
                <td class="table_1">{{$payment->quantity}}</td>
                <td class="table_1">{{$payment->value_ref}}</td>
                <td class="table_1">{{$payment->assignment}}</td>
                <td class="table_1">{{$payment->deduction}}</td>
                <td class="table_1">{{$payment->balance}}</td>
            </tr>
        @endforeach
    @else
        <p>Usted no tiene pagos en este periodo</p>
    @endif
    </table>
    <br><br><br>
    <table class="table_1">
        <tr>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td class="table_1"></td>
        </tr>
        <tr>
            <td colspan ="5" class="table_1"><strong>Totales</strong></td>
            <td class="table_1">{{$overall['assignment']}}</td>
            <td class="table_1">{{$overall['deduction']}}</td>
            <td class="table_1"></td>
        </tr>
        <tr>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
            <td class="table_space"></td>
        </tr>
        <tr>
            <th colspan="4" class="text-left table_3"><strong>Observaciones:<strong></th>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <th colspan="2" class="table_2"><strong>Neto Depositado:</strong></th>
        </tr>
        <tr>
            <td colspan="4" class="text-left table_3">{{$date['observation']}}</td>
            <td class="table_1"></td>
            <td class="table_1"></td>
            <td colspan="2" class="table_2">{{$overall['grandson']}}</td>
        </tr>
    </table>
    <br><br><br>

</body>
</html>