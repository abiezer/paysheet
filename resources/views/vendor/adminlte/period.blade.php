@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
		
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Seleccione el periodo a Consultar</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						
						<label>Desde  /  Hasta</label>
						<select name="period" class="form-control" id="period">
							@foreach($periods as $period)
								<option value="{{$period->period}}-{{$period->year}}">{{$period->description}}  {{$period->year}}</option>
							@endforeach
						</select>
						<br>
						<a type="submit" class="btn btn-success" id="consult">Consultar</a>
					
					</div>
					<!--/.box-body-->
				</div>
				<!-- /.box -->
			</div>
				
		</div>
	</div>
@endsection
