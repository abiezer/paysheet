@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
		
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Seleccione el periodo a Consultar</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<label>Tipo de nomina</label>
						<select class="form-control" id="type_paysheet">
                            <option value=""></option>
                            <option value="d">Diaria</option>
                            <option value="m">Mensual</option>
                        </select>
                        <div id="response_since"></div>
                        <div id="response_until"></div>
					</div>
					<!--/.box-body-->
				</div>
				<!-- /.box -->
			</div>
				
		</div>
	</div>
@endsection
