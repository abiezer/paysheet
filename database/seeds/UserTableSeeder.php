<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'name'                       =>  'whithout user',
        //     'email'                      =>  'whithoutuser@cabelum.com.ve',
        //     'password'                   =>  bcrypt('2580abiezer'),
        // ]);

        DB::table('employees')->insert([
            'identification'    =>  '21264363',
            'name'              =>  'Abiezer Jose Sifontes Quijada',
            'index_card'        =>  '3764',
            'unity'             =>  'Gerencia de Tecnologia Informatica',
            'appoinment'        =>  'Analista de Sistemas',
            'salary'            =>  '73.806',
            'bank'              =>  'Venezuela',
            'bank_account'      =>  '01020414310000129897',
            'type_paysheet'     =>  '1',
            'join_date'         =>  '2017-02-13',
        ]);

        DB::table('users')->insert([
            'name'                       =>  'Abiezer',
            'email'                      =>  'asifontes@cabelum.com.ve',
            'employee_identification'    =>  '21264363',
            'password'                   =>  bcrypt('2580abiezer')
        ]);

        
         
        // DB::table('payments')->insert([
        //     'nro_receipt'               =>  '123',
        //     'since'                     =>  '2017-03-01',
        //     'until'                     =>  '2017-03-15',
        //     'code'                      =>  '11',
        //     'concept'                   =>  'Dias Trabajados',
        //     'surcharge'                 =>  'Salario B.',
        //     'quantity'                  =>  '11',
        //     'value_ref'                 =>  '2460',
        //     'assignment'                =>  '27062',
        //     'deduction'                 =>  '0',
        //     'balance'                   =>  '0',
        //     'employee_identification'   =>  '21264363'
        // ]);
        
        // DB::table('payments')->insert([
        //     'nro_receipt'               =>  '123',
        //     'since'                     =>  '2017-03-01',
        //     'until'                     =>  '2017-03-15',
        //     'code'                      =>  '53',
        //     'concept'                   =>  'Subsidios de Vivienda',
        //     'surcharge'                 =>  '',
        //     'quantity'                  =>  '0',
        //     'value_ref'                 =>  '0',
        //     'assignment'                =>  '1350',
        //     'deduction'                 =>  '0',
        //     'balance'                   =>  '0',
        //     'employee_identification'   =>  '21264363'
        // ]);
    }
}
