<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->integer('identification')->primary()->unique()->unsigned();
            $table->string('name');
            $table->integer('index_card');
            $table->string('unity');
            $table->string('appoinment');
            $table->float('salary');
            $table->string('bank');
            $table->string('bank_account');
            $table->integer('type_paysheet');
            $table->date('join_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
