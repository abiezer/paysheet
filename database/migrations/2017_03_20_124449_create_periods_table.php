<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period');
            $table->integer('year');
            $table->date('since');
            $table->date('until');
            $table->enum('type_paysheet',['d','m']);
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::unprepared("ALTER TABLE periods CHANGE period period INT(2)ZEROFILL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periods');
    }
}
