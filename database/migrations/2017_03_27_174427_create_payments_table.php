<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('concept');
            $table->string('surcharge');
            $table->string('quantity');
            $table->string('value_ref');
            $table->string('assignment');
            $table->string('deduction');
            $table->string('balance');
            $table->integer('period_id')->unsigned();
            $table->integer('employee_identification')->unsigned();
            $table->foreign('employee_identification')->references('identification')->on('employees');
            $table->foreign('period_id')->references('id')->on('periods');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
