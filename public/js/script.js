$(document).ready(function(){
    var URLactual = window.location.host;
    
    if($('#consult').length>0){
        $.get("first_link",function(period){
            $('#consult').attr('href','http://'+URLactual+'/receipt/'+period['period']+'/'+period['year']);
        });
    }

    $(document).on("change","#period",function(){
        var period = $(this).val().split("-");
        $('#consult').attr('href','http://'+URLactual+'/receipt/'+period[0]+'/'+period[1]);
    });

    $(document).on("change","#type_paysheet",function(){
        url ="get_periods_import/"+$(this).val();
        $.get(url,function(data){
            $("#response_since").html("<label>Desde</label><select class='form-control' id='since'></select>");
            for(i=0;i<data.length;i++){
                $("#since").append("<option value='"+data[i]['period']+"-"+data[i]['year']+"'>"+data[i]['description']+" "+data[i]['year']+"</period>");
            }
            $("#response_until").html("<label>Hasta</label><select class='form-control' id='until'></select>");
            for(i=0;i<data.length;i++){
                $("#until").append("<option value='"+data[i]['period']+"-"+data[i]['year']+"'>"+data[i]['description']+" "+data[i]['year']+"</period>");
            }
        });
    });
});